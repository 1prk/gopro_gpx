import ffmpeg
import gpmf
import gpxpy
import os
from pandas import DataFrame
import argparse

parser = argparse.ArgumentParser(description='GoPro GPS-Extractor')
parser.add_argument('-p', '--path', type= str, required=True, help= 'specify path where GoPro video files are located')
args = parser.parse_args()

ext = [".mp4", ".360"]
#look for all video files in specific directory
for root, dirs, files in os.walk(args.path):
    for file in files:
        if file.endswith(tuple(ext)):
            try:
                stream = gpmf.io.extract_gpmf_stream(os.path.join(root, file))
                gps_blocks = gpmf.gps.extract_gps_blocks(stream)
                gps_data = list(map(gpmf.gps.parse_gps_block, gps_blocks))

                gpx = gpxpy.gpx.GPX()
                gpx_track = gpxpy.gpx.GPXTrack()
                gpx.tracks.append(gpx_track)
                gpx_track.segments.append(gpmf.gps.make_pgx_segment(gps_data))

                print("{} track(s)".format(len(gpx.tracks)))
                track = gpx.tracks[0]

                print("{} segment(s)".format(len(track.segments)))
                segment = track.segments[0]

                print("{} point(s)".format(len(segment.points)))

                data = []
                segment_length = segment.length_3d()
                for point_idx, point in enumerate(segment.points):
                    data.append([point.longitude, point.latitude,
                                 point.elevation, point.time, segment.get_speed(point_idx)])

                columns = ['Longitude', 'Latitude', 'Altitude', 'Time', 'Speed']
                df = DataFrame(data, columns=columns)
                print("CSV file saved as {}".format(os.path.join(root, file)+".csv"))
                df.to_csv(os.path.join(root, file)+".csv")

                with open(os.path.join(root, file)+".gpx", "w") as f:
                    f.write(gpx.to_xml())
            except ffmpeg.Error as e:
                print("no GPS data found in file {}".format(os.path.join(root, file)))

            finally:
                print('done!')

