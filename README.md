# gopro_gpx

simple python CLI tool for extracting GPMF telemetry data from gopro video files (HERO 7+, 360). 
It will look for `.mp4` and `.360` video files in specified (sub-)directories and create `.csv` and `.gpx` files with their corresponding telemetry data (latitude, longitude, speed, elevation, heading).

## installation & dependencies

run the following in your favorite working directory

`git clone https://gitlab.com/1prk/gopro_gpx`

this CLI tool runs on following python packages. run the following command to obtain them (use `pip3` if you're running python 3)

`pip install gpxpy gpmf ffmpeg-python pandas`

## how to use

run the following command in your terminal

linux:
`./main.py -p <PATH>`

windows:
`python3 main.py -p <PATH>`

with `<PATH>` pointing to the video directory

## troubleshooting

if you encounter the following ffmpeg errors like these:

`ffmpeg._run.Error: ffprobe error (see stderr output for detail)`

run this:

```
pip uninstall python-ffmpeg
pip install ffmpeg-python
```

and run the CLI tool again.

## thanks to

alexis-mignon for providing the python GPMF wrapper
https://github.com/alexis-mignon/pygpmf

all contributors of gpxpy
https://github.com/tkrajina/gpxpy
